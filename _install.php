<html>
<head>
  	<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"/></script>
  	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Lato:100,400,300,700" type="text/css"/>
  	<link rel="stylesheet" href="app/css/login-style.css" media="screen" type="text/css" />
	<link type="text/css" rel="stylesheet" href="./app/css/style.css"/>
	<title>Install timen</title>
</head>
<body>
	<div class="login-card">
	    <h1>Install timen</h1><br>
	  <form action="_install.php" type="POST">
	    <input type="text" name="db_host" placeholder="db_host (localhost)">
	    <input type="text" name="db_user" placeholder="db_username">
	    <input type="password" name="db_pass" placeholder="Password">
	    <input type="text" name="db_name" placeholder="db_name">
	    <input type="submit" name="install" class="login login-submit" value="install">
	  </form>
		<?php
		if (!empty($_GET["install"])){
			$dbhost = $_GET["db_host"];
			$dbuser = $_GET["db_user"];
			$dbpass = $_GET["db_pass"];
			$conn = mysql_connect($dbhost, $dbuser, $dbpass);
			if(! $conn )
			{
			  die('Could not connect: ' . mysql_error());
			}
			echo 'Connected successfully<br>';
			mysql_select_db( $_GET["db_name"] );

			$time_entries = "CREATE TABLE time_entries( ".
			       	"entry_id INT NOT NULL AUTO_INCREMENT, ".
			       	"entry_user VARCHAR(100) NOT NULL, ".
			       	"entry_project VARCHAR(100) NOT NULL, ".
			       	"entry_client VARCHAR(100) NOT NULL, ".
			       	"entry_start VARCHAR(100) NOT NULL, ".
			       	"entry_end VARCHAR(100) NOT NULL, ".
			       	"entry_complete VARCHAR(100) NOT NULL, ".
			       	"entry_date DATE, ".
			       	"PRIMARY KEY ( entry_id )); ";
			$create_time_entries = mysql_query( $time_entries, $conn );
			if(! $create_time_entries )	{
			  echo "Could not create table time_entries:<br>" . mysql_error()."<br>";
			}

			$clients = "CREATE TABLE clients( ".
			       	"client_id INT NOT NULL AUTO_INCREMENT, ".
			       	"client_name VARCHAR(100) NOT NULL, ".
			       	"PRIMARY KEY ( client_id )); ";
			$create_clients = mysql_query( $clients, $conn );
			if(! $create_clients )	{
			  echo "Could not create table clients:<br>" . mysql_error()."<br>";
			}

			$users = "CREATE TABLE users( ".
			       	"user_id int(11) NOT NULL AUTO_INCREMENT COMMENT 'auto incrementing user_id of each user, unique index', ".
			       	"user_name varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT 'user''s name, unique', ".
					"user_password_hash varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'user''s password in salted and hashed format', ".
					"user_email varchar(64) COLLATE utf8_unicode_ci NOT NULL COMMENT 'user''s email, unique', ".
					"PRIMARY KEY ( user_id ), ".
					"UNIQUE KEY user_name ( user_name ), ".
				  	"UNIQUE KEY user_email ( user_email ) ) ".
					"ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='user data'; ";
			$create_users = mysql_query( $users, $conn );
			if(! $create_clients )	{
			  echo "Could not create table users:<br>" . mysql_error()."<br>";
			}

			echo "Tables created successfully<br>You can strat tracking.";
			mysql_close($conn);
		}
		?>
	</div>
<script type="text/javascript" src="./app/js/script.js"></script>
</body>
</html>