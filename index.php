<?php
require_once("app/core.php");
?>
<!DOCTYPE html>
<html>
<head>
  	<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"/></script>
  	<link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Lato:100,400,300,700"/>
  	<link type="text/css" rel="stylesheet" href="app/css/login-style.css" media="screen" />
	<link type="text/css" rel="stylesheet" href="./app/css/reset.css"/>
	<link type="text/css" rel="stylesheet" href="./app/css/style.css"/>
	<!-- jQuery simplemodal.js -->
	<script src='./app/js/jquery.simplemodal.js' type='text/javascript'></script>
	<title>timen</title>
</head>
<body>
<!-- tracking area -->
<div id="wrapper">
<?php
// ... ask if we are logged in here:
if ($login->isUserLoggedIn() == true) {
$logged_User = $_SESSION['user_name'];
?>
	<div id="user-area">
	<?php echo $logged_User; ?> |	<a href="index.php?logout">Logout</a>
	</div>
	<div id="project-input" class="clearfix">
	<input type="text" name="client-name" placeholder="Client" class="client-name radius-2px bottom-border">
	<input type="text" name="project-name" placeholder="Project" class="project-name radius-2px bottom-border">
	</div>
	<div id="time" class="clearfix">
		<span class="days">00:</span>
		<span class="hours">00:</span>
		<span class="minutes">00:</span>
		<span class="seconds">00</span>
		<span class="minus-pause"></span>
	</div>
	<div id="tracking-buttons" class="clearfix">
		<div class="tracking border radius-2px">START</div>
		<div class="pause border radius-2px">PAUSE</div>
	</div>
	<div id="entries"></div>
</div>
<!-- modal content -->
<div id="simple-modal" class="soft-shadow" style="display: none;">
	<div class="inner">
		<img id="modal-close" src="./app/cross-icon.svg" class="icon cross">

	</div>
<!-- End your code here -->
<script type="text/javascript" src="./app/js/script.js"></script>
<script type="text/javascript">
	//$( "#entries" ).load( "app/core.php?q=getEntries" );
	getEntries("desc");
</script>
<?php
} else {
    include("app/php-login/views/not_logged_in.php");
?>
<?php
}
?>
</div>
</body>
</html>