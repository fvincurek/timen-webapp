/* Write JavaScript here */
var start, end, timer, i, diffHrs, diffMins, diffSecs, pausedStart, pausedTime, pausedEnd, entryProject, globalSortWay, globalSortBy;

$( "#tracking-buttons > .pause" ).addClass("disabled");

// GET CLIENT NAME
  $( "#project-input > .project-name" ).keyup(function(event) {
    entryProject = $( "#project-input > .project-name" ).val();
  });

// TRACKING BUTTON HANDLER
  $( "#tracking-buttons > .tracking" ).click(function() {
    var buttonState = $("#tracking-buttons > .tracking").hasClass("active");
    if (!buttonState){
    // START TRACKING
      entryProject = $( "#project-input > .project-name" ).val();
      entryClient = $( "#project-input > .client-name" ).val(); 
      if (!entryProject){
        alert("Please name your entry");
      } else {
      if (!pausedStart){ start = new Date(); } else { start = pausedStart; }
      $.get( "app/core.php", 
        { q: "addEntry", 
          entry_start: start,
          entry_project: entryProject,
          entry_client: entryClient,
          entry_date: "Today@2pm" 
        } )
        .done(function( data ) {
          timer = setInterval(function(){myTimer()}, 1000);
          $("#tracking-buttons > .tracking").html("STOP").addClass("active");
          setCookie('currentEntry',data,'1');
          $( "#tracking-buttons > .pause" ).removeClass("disabled");
          getEntries();
        });
      }
    } else {
    // STOP TRACKING
      var currentEntry = getCookie('currentEntry');
      $.get( "app/core.php", 
        { q: "updateEntry", 
          entry_end: end, 
          entry_id: currentEntry
        } )
        .done(function( data ) {
          $("#tracking-buttons > .tracking").html("START").removeClass("active");
          $("#time > .hours").html("00:");
          $("#time > .minutes").html("00:");
          $("#time > .seconds").html("00");
          deleteCookie('currentEntry');
          $("#time").removeClass("blink");
          $("#tracking-buttons > .pause").html("PAUSE").removeClass("active");
          $("#tracking-buttons > .pause").addClass("disabled");
          getEntries();
          clearTimer();
        });
    }
  });

// PAUSE BUTTON HANDLER
  $( "#tracking-buttons > .pause" ).click(function() {
    var buttonState = $("#tracking-buttons > .tracking").hasClass("active");
    if (buttonState){
      var buttonState = $("#tracking-buttons > .pause").hasClass("active");
      if (!buttonState){
        clearInterval(timer);
        $("#time").addClass("blink");
        $("#tracking-buttons > .pause").html("CONTINUE").addClass("active");
        pausedStart = start;
        pausedTime = new Date();
      } else {
        $("#time").removeClass("blink");
        $("#tracking-buttons").children(".pause").html("PAUSE").removeClass("active");
        pausedEnd = +pausedEnd + +(new Date() - pausedTime);
        timer = setInterval(function(){myTimer()}, 1000);
      }
    }
  });

// timing function
  function myTimer() {
    //alert(pausedEnd);
    if (!pausedEnd){ pausedEnd = "";};
    end = (new Date() - start) - pausedEnd;
    var values = convertMS(end); 
    $("#time > .hours").html(values.h+":");
    $("#time > .minutes").html(values.m+":");
    $("#time > .seconds").html(values.s);
  }
  function clearTimer(){
    pausedTime = false;
    pausedEnd = false;
    pausedStart = false;
    clearInterval(timer);
  }
  function convertMS(ms) {
    var d, h, m, s;
    s = Math.floor(ms / 1000);
    m = Math.floor(s / 60);
    s = s % 60; if(s<10){s="0"+s};
    h = Math.floor(m / 60);
    m = m % 60; if(m<10){m="0"+m};
    d = Math.floor(h / 24);
    h = h % 24; if(h<10){h="0"+h};
    return { d: d, h: h, m: m, s: s };
  };
  Date.prototype.today = function () { 
      return ((this.getDate() < 10)?"0":"") + this.getDate() +
          "/"+(((this.getMonth()+1) < 10)?"0":"") + (this.getMonth()+1) +
          "/"+ this.getFullYear();
  };
  Date.prototype.timeNow = function () {
      return ((this.getHours() < 10)?"0":"") + this.getHours() +
      ":"+ ((this.getMinutes() < 10)?"0":"") + this.getMinutes() +
      ":"+ ((this.getSeconds() < 10)?"0":"") + this.getSeconds();
  };

// lload entries
function getEntries(givenSortBy, givenSortWay, client, project){
  $( "#entries" ).html( '...loading...' );
  
  // send request to the server
  $.ajax({
    type: 'GET',
    url: 'app/core.php?q=getEntries',
    data: {},
    dataType: 'json',
    async: true, //This is deprecated in the latest version of jquery must use now callbacks
    success: function(entries){
      var allEntries = [];
      entries.forEach(function(entry) {
        var str = entry;
        var res = str.split("|");
        var user = res[0];
        var project = res[1];
        var duration = res[2];
        var srcDuration = res[3];
        var client = res[4];

        var singleEntry = {entry_user:user, entry_project:project, entry_duration:duration, src_duration: srcDuration, entry_client: client};
        allEntries.push(singleEntry); 
      });

      globalSortWay = typeof globalSortWay !== "undefined" ? globalSortWay : "duration_asc";
      givenSortWay = typeof givenSortWay !== "undefined" ? givenSortWay : globalSortWay;
      
      if (givenSortWay === "duration_asc"){ // sort ASC
        allEntries.sort(function(a, b){ return a.src_duration-b.src_duration });
        revertSort = "duration_desc";
        globalSortWay = "duration_asc";
      } else if (givenSortWay === "duration_desc"){ // sort DESC
        allEntries.sort(function(a, b){ return b.src_duration-a.src_duration });
        revertSort = "duration_asc";
        globalSortWay = "duration_desc";
      }

      $( "#entries" ).html( '<span id="sort-button" onClick="getEntries(\'\', \'' + revertSort + '\')">Sort by: '+givenSortWay+'</span>' );
      allEntries.forEach(function(entry) {
        var toHtml = '<div id="single-entry" class="shadow-soft border radius-2px clearfix">' +
                        '<span style="float: left;"> ' + entry.entry_project + ' </span>' +
                        '<span style="float: left;"> ' + entry.entry_client + ' </span>' +
                        '<span style="float: right;"> ' + entry.entry_duration + ' </span>' +
                        '<span style="float: right;"> ' + entry.entry_user + ' </span>' +
                      '</div><!-- .entry for ' + entry.entry_user + ' -->';
        $( "#entries" ).append( toHtml );
      });

    },
    onError: function(error) {
        alert('code: '    + error.code    + '\n' +
              'message: ' + error.message + '\n');
    }
    });
}

// cookie functions
  function setCookie(cname,cvalue,exdays) {
      var d = new Date();
      d.setTime(d.getTime()+(exdays*24*60*60*1000));
      var expires = "expires="+d.toGMTString();
      document.cookie = cname + "=" + cvalue + "; " + expires;
  }

  function getCookie(c_name) {
      var i,x,y,ARRcookies=document.cookie.split(";");
      for (i=0;i<ARRcookies.length;i++)
      {
          x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
          y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
          x=x.replace(/^\s+|\s+$/g,"");
          if (x==c_name)
          {
              return unescape(y);
          }
      }
  }

  function deleteCookie(c_name) {
      document.cookie = encodeURIComponent(c_name) + "=deleted; expires=" + new Date(0).toUTCString();
  }

// modals
  $(document).ready(function () {
      $( "#modal-close" ).click(function() {
          $.modal.close();
      });
  })

  //$('#simple-modal').modal();