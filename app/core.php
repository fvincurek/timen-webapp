<?php
/*
 *	Core functionality
 *	settings, DB, storage, etc
 *	Change things below only if you know 
 *	what you're doing.
 *
 */

require_once("dbConfig.php");
// include script for working with MYSQL(i) and connect
require_once("MysqliDb.php");
$db = new MysqliDb(DB_HOST, DB_USER, DB_PASS, DB_NAME);
require_once("php-login/init.php");
$login = new Login();

if ($login->isUserLoggedIn() == true) {
$logged_User = $_SESSION['user_name'];

// insert entry into DB
function addEntry($db, $for){
	$data = Array ("entry_start" => $_GET["entry_start"], 
			"entry_date" => $_GET["entry_date"], 
			"entry_project" => $_GET["entry_project"],
			"entry_client" => $_GET["entry_client"], 
			"entry_user" => $for);
	$entry = $db->insert('time_entries', $data);
	if($entry) echo $entry;
}

// update last entry in DB
function updateEntry($db){
	$data = Array ("entry_end" => $_GET["entry_end"], "entry_complete" => "1");
	$db->where ('entry_id', $_GET["entry_id"]);
	if ($db->update ('time_entries', $data)){
	    //echo $db->count . ' records were updated';
	} else {
	    echo 'update failed: ' . $db->getLastError();
	}
}

// get entries from DB
function getEntries($db, $for){
	$db->where ('entry_user', $for);
	$db->orderBy("entry_id","desc");
	$cols = Array ("entry_id", "entry_start", "entry_end", "entry_date", "entry_complete", "entry_project", "entry_client");
	$all_entries = $db->get('time_entries', null, $cols);

	$result = array();
	foreach ($all_entries as $entry) {
	// generate string for javascript
		$entry_src_dur = $entry["entry_end"]/1000;
		$entry_dur = gmdate("H:i:s", $entry_src_dur);
		$whole_pack = $for."|".$entry["entry_project"]."|".$entry_dur."|".$entry_src_dur."|".$entry["entry_client"]; 
		array_push($result, $whole_pack);
	}
	echo json_encode($result);
}

if (isset($_GET['q'])){
	if ($_GET['q'] == "addEntry"){ addEntry($db, $logged_User); }
	if ($_GET['q'] == "updateEntry"){ updateEntry($db); }
	if ($_GET['q'] == "getEntries"){ getEntries($db, $logged_User); }
}
}
?>